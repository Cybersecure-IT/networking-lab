en
conf t
hostname A1
vlan 10
vlan 20

int g0/2
switchport mode access
switchport access vlan 10

int g0/3
switchport mode access
switchport access vlan 20

end
write