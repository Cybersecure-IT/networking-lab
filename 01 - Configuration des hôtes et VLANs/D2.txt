en
conf t
hostname D2
vlan 10
vlan 20

int range g0/0-2
switchport trunk encapsulation dot1q
switchport mode trunk

end
write